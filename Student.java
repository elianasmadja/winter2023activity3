import java.util.Random;
public class Student{
    public String program;
    public int age;
    public int gradYear;
    
    public void gradCapsUp(int graduationYear){
        int yearsTillGrad = graduationYear - 2022;
        for(int i = yearsTillGrad; i <= yearsTillGrad && i > 0; i--) {
            System.out.println(i + " years until graduation");
        }
        System.out.println("Throw your cap! You did it!");
    }
    
    public void goOrSkip(){
        Random rand  = new Random();
        int value = rand.nextInt(2);
        if(value == 0) {
            System.out.println("We're going to class today!");
        }
        else {
            System.out.println("We're skipping class today!");
        }
    }
}

