public class Application{
    public static void main(String[]args) {
        Student student1 = new Student();
        student1.age = 17;
        student1.gradYear = 2026;
        student1.program = "Computer Science";
        //System.out.println(student1.program);
        
        //Student.gradCapsUp(student1.gradYear);
        
        Student student2 = new Student();
        student2.age = 20;
        student2.gradYear = 2024;
        student2.program = "Health Science";
        //System.out.println(student2.gradYear);
        
        Student students = new Student();
        //students.gradCapsUp(student1.gradYear);
        //students.gradCapsUp(student2.gradYear);
        //students.goOrSkip();
        
        Student[] section3 = new Student[3];
        section3[0] = student1;
        section3[1] = student2;
        
        //System.out.println(section3[0].age);
        //System.out.println(section3[2].age);
        
        section3[2] = new Student();
        section3[2].age = 22;
        section3[2].gradYear = 2023;
        section3[2].program = "Nursing";
        
        System.out.println(section3[2].age);
        System.out.println(section3[2].gradYear);
        System.out.println(section3[2].program);
    } 
}